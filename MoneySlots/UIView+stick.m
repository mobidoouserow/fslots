//
//  UIView+Pin.m
//  FSlots
//
//  Created by Pavel Wasilenko on 12.06.17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

#import "UIView+stick.h"

@implementation UIView (stick)

- (void)pinAllAtributesToView:(UIView *)view constant:(CGFloat)constant;
{
    NSArray *attributes =
    @[
      @(NSLayoutAttributeTop),
      @(NSLayoutAttributeBottom),
      @(NSLayoutAttributeLeading),
      @(NSLayoutAttributeTrailing),
    ];
    
    [self pinToView:view attributes:attributes constant:constant];
}

- (void)pinToView:(UIView *)view attributes:(NSArray *)attributes constant:(CGFloat)constant;
{
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    for (NSNumber *attrib in attributes) {
        
        NSLayoutConstraint *constraint =
        [NSLayoutConstraint constraintWithItem:self
                                     attribute:attrib.integerValue
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:view
                                     attribute:attrib.integerValue
                                    multiplier:1.0f
                                      constant:constant];
        
        [view addConstraint:constraint];
    }
}

@end
