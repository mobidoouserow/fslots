//
//  UIView+Pin.h
//  FSlots
//
//  Created by Pavel Wasilenko on 12.06.17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (stick)

- (void)pinAllAtributesToView:(UIView *)view constant:(CGFloat)constant;

- (void)pinToView:(UIView *)view attributes:(NSArray *)attributes constant:(CGFloat)constant;

@end
