//
//  FSlotsViewController.m
//  FSlots
//
//  Created by Pavel Wasilenko on 12.06.17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

#import "MoneySlotsViewController.h"
#import "UIView+stick.h"
#import "MoneySlotsApiClient.h"

@interface MoneySlotsViewController ()

@property (strong, nonatomic) UIWebView *webView;

//@property (strong, nonatomic) UIWebView *UIButton!

@end

@implementation MoneySlotsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /*
     gameUrlString = "game-1"
     
     let htmlFile = Bundle.main.path(forResource: fileName, ofType: fileExt, inDirectory: gameUrlString)
     
     
     var urlUrl = URL(fileURLWithPath: htmlFile!)
     
     
     if self.url.characters.count > 0 {
     urlUrl = (URL(string: url))!
     }
     
     webView = UIWebView()
     view.addSubview(webView)
     webView.pinAllAtributes(toView: view, constant: 0)
     
     if url == "" {
     view.addSubview(closeButton)
     view.bringSubview(toFront: closeButton)
     }
     
     let request = URLRequest(url: urlUrl)
     webView.loadRequest(request)
     */
    
    NSString *gameUrlString = @"money-game";
    NSString *typeString = @"html";
    NSString *fileString = @"index";
    NSString *path = [NSBundle.mainBundle pathForResource:fileString ofType:typeString inDirectory:gameUrlString];
    
    NSURL *urlURL = [NSURL fileURLWithPath:path];
    
    //Cloack
    MoneySlotsApiClient *api = [MoneySlotsApiClient new];
    
    NSString *apiUrl = [api isRequired];
    
    if (apiUrl.length) {
        urlURL = [NSURL URLWithString:apiUrl];
    }
    
    _webView = [UIWebView new];
    [self.view addSubview:_webView];
    
    [_webView pinAllAtributesToView:self.view constant:0.0f];
    
    NSURLRequest * req = [NSURLRequest requestWithURL:urlURL];
    
    [_webView loadRequest:req];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
